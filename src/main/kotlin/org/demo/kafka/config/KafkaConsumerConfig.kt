package org.demo.kafka.config

import io.confluent.kafka.serializers.KafkaAvroDeserializer
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.core.DefaultKafkaConsumerFactory


@Configuration
class KafkaConsumerConfig(@Value("\${kafka.brokers}") val brokers: String) {

//    @Bean
//    fun consumerFactory(): DefaultKafkaConsumerFactory<String, Any> {
//
//        val props: MutableMap<String, Any> = HashMap()
//        props[ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG] = brokers
//        props[ConsumerConfig.GROUP_ID_CONFIG] = "group_id"
//        props[ConsumerConfig.AUTO_OFFSET_RESET_CONFIG] = "latest"
//        props[ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.java
//        props[ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG] =  KafkaAvroDeserializer::class.java.name
//        props["schema.registry.url"] = "http://127.0.0.1:8081"
//        props[KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG] = "true"
//
//        val avroDeser = KafkaAvroDeserializer()
//        avroDeser.configure(props, false)
//
//        return DefaultKafkaConsumerFactory(props, StringDeserializer(), avroDeser)
//    }
//
//    @Bean
//    fun kafkaListenerContainerFactory(): ConcurrentKafkaListenerContainerFactory<String, org.demo.kafka.Employee> {
//        val factory = ConcurrentKafkaListenerContainerFactory<String, org.demo.kafka.Employee>()
//        factory.consumerFactory = consumerFactory()
//        return factory
//    }
}