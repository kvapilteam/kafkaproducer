package org.demo.kafka.config

import io.confluent.kafka.serializers.KafkaAvroSerializer
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.core.ProducerFactory


@Configuration
class KafkaProducerConfig(@Value("\${kafka.brokers}") val brokers: String) {

//    @Bean
//    fun producerFactory(): ProducerFactory<String, org.demo.kafka.Employee> {
//        val configProps: MutableMap<String, Any> = HashMap()
//        configProps[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = brokers
//        configProps[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
//        configProps[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] =  KafkaAvroSerializer::class.java.name
//        configProps[ProducerConfig.ACKS_CONFIG] = "all"
//        configProps["schema.registry.url"] = "http://127.0.0.1:8081"
//        configProps["spring.kafka.producer.auto.register.schemas"] = false
//        return DefaultKafkaProducerFactory(configProps)
//    }
//
//    @Bean
//    fun kafkaTemplate(): KafkaTemplate<String, org.demo.kafka.Employee> {
//        return KafkaTemplate(producerFactory())
//    }

//    @Bean
//    fun topicUser(): NewTopic = NewTopic("employee", 3, 1)
}