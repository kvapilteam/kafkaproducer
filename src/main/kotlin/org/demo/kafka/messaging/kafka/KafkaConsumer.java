package org.demo.kafka.messaging.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.demo.kafka.Employee;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

 // @KafkaListener(topics = "employee", groupId = "group_id")
  public void consumeMessage(ConsumerRecord<String, Employee> message){

    Employee employee = message.value();

    System.out.println("message:" + employee.getName() + " " + employee.getAge());
  }
}
