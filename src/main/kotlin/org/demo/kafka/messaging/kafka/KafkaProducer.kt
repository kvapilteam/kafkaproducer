package org.demo.kafka.messaging.kafka

import org.demo.kafka.rest.UserRequest
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import java.util.*

@Component
class KafkaProducer(val kafkaTemplate: KafkaTemplate<String, org.demo.kafka.Employee>) {

    fun sendMessage(user : UserRequest) {
        val avroUser = org.demo.kafka.Employee()
        avroUser.setName(user.name)
        avroUser.setAge(user.age)
        avroUser.setId(UUID.randomUUID().toString())
        avroUser.setRegion("X")

       //ProducerRecord<String, User>
       // kafkaTemplate.send()
        kafkaTemplate.send("employee", user.name, avroUser)
    }
}