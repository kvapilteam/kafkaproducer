package org.demo.kafka.rest

import org.demo.kafka.messaging.kafka.KafkaProducer
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class KafkaController(val kafkaProducer: KafkaProducer) {

    @PostMapping("/publish")
    fun sendMessageToKafkaTopic(@RequestBody request: UserRequest) {
        kafkaProducer.sendMessage(request)
    }
}